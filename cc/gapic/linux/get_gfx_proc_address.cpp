/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../dl_loader.h"
#include "../log.h"

namespace gapic {

void* GetGfxProcAddress(const char *name, bool bypassLocal) {
    typedef void* (*GPAPROC)(const char *name);

    if (bypassLocal) {
        static DlLoader libgl("libGL.so");
        if (GPAPROC gpa = reinterpret_cast<GPAPROC>(libgl.lookup("glXGetProcAddress"))) {
            if (void* proc = gpa(name)) {
                GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (via libGL glXGetProcAddress)\n", name, bypassLocal, proc);
                return proc;
            }
        }
        if (void* proc = libgl.lookup(name)) {
            GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (from libGL dlsym)\n", name, bypassLocal, proc);
            return proc;
        }
    } else {
        static DlLoader local(nullptr);
        if (GPAPROC gpa = reinterpret_cast<GPAPROC>(local.lookup("glXGetProcAddress"))) {
            if (void* proc = gpa(name)) {
                GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (via local glXGetProcAddress)\n", name, bypassLocal, proc);
                return proc;
            }
        }
        if (void* proc = local.lookup(name)) {
            GAPID_INFO("GetGfxProcAddress(%s, %d) -> 0x%x (from local dlsym)", name, bypassLocal, proc);
            return proc;
        }
    }

    GAPID_INFO("GetGfxProcAddress(%s, %d) -> not found\n", name, bypassLocal);
    return nullptr;
}

}  // namespace gapic

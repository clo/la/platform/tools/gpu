{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{/* ---- Includes ---- */}}
{{Include "cpp_common.tmpl"    }}

{{$ | Macro "gfx_api.cpp" | Format (Global "clang-format") | Write "gfx_api.cpp"}}

{{/*
-------------------------------------------------------------------------------
  Entry point.
-------------------------------------------------------------------------------
*/}}
{{define "gfx_api.cpp"}}
  {{AssertType $ "API"}}

  {{template "C++.GeneratedHeader"}}
¶
  #include "gfx_api.h"
  #include "interpreter.h"
  #include "stack.h"
¶
  #include <gapic/get_gfx_proc_address.h>
  #include <gapic/log.h>
¶
  #define __STDC_FORMAT_MACROS
  #include <inttypes.h>
¶
  namespace gapir {
  namespace gfxapi {
  namespace {
¶
  {{range $c := AllCommands $}}
    {{if not (GetAnnotation $c "synthetic")}}
      {{template "CommandHandler" $c}}
    {{end}}
  {{end}}
¶
  }  // end of anonymous namespace
¶
  {{range $c := AllCommands $}}
    {{if not (GetAnnotation $c "synthetic")}}
      {{template "C++.FunctionPtrDecl" $c}} = nullptr;
    {{end}}
  {{end}}
¶
  {{template "Register" $}}
  {{template "Initialize" $}}
¶
  }  // namespace gfxapi
  }  // namespace gapir
¶
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the Register function that registers all the functions with the
  interpreter.
-------------------------------------------------------------------------------
*/}}
{{define "Register"}}
  {{AssertType $ "API"}}

  void Register(Interpreter* interpreter) {
    {{range $c := AllCommands $}}
      {{if not (GetAnnotation $c "synthetic")}}
        {{$name := Macro "C++.Public" (Macro "CmdName" $c)}}
        interpreter->registerFunction(Ids::{{$name}}, call{{$name}});
      {{end}}
    {{end}}
  }
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the Initialize function that looks-up the API function addresses for
  the currently bound context.
-------------------------------------------------------------------------------
*/}}
{{define "Initialize"}}
  {{AssertType $ "API"}}

  void Initialize() {
    {{range $c := AllCommands $}}
      {{if not (GetAnnotation $c "synthetic")}}
        {{$name := Macro "CmdName" $c}}
        {{$name}} = reinterpret_cast<{{Template "C++.FunctionPtrType" $c}}>(gapic::GetGfxProcAddress("{{$name}}", false));
      {{end}}
    {{end}}
  }
{{end}}


`{{define "CommandHandler"}}
  {{AssertType $ "Function"}}

  {{$name := Macro "CmdName" $}}

  bool call{{Template "C++.Public" $name}}(Stack* stack, bool pushReturn) {
    {{range $p := (Reverse $.CallParameters)}}
      {{Template "C++.ParameterType" $p}} {{$p.Name}} = stack->pop<{{Template "C++.ParameterType" $p}}>();
    {{end}}

    if (stack->isValid()) {
    GAPID_INFO({{Template "C++.PrintfCommandCall" $}});

      if ({{$name}} != nullptr) {
        {{if not (IsVoid $.Return.Type)}}
          {{Template "C++.ReturnType" $}} return_value = {{Template "Call" $}};
          GAPID_INFO("Returned: {{Template "C++.PrintfFormatCode" $.Return.Type}}\n", return_value);
          if (pushReturn) {
            stack->push<{{Template "C++.ReturnType" $}}>(return_value);
          }
        {{else}}
          {{Template "Call" $}};
        {{end}}

        {{if 1}} {{/* Enable for debugging */}}
          const GLenum err = glGetError();
          if (err != GLenum::GL_NO_ERROR) {
            GAPID_WARNING("{{$name}} returned error: 0x%x\n", err);
          }
        {{end}}
      } else {
        GAPID_WARNING("Attempted to call unsupported function {{$name}}\n");
      }

      return true;
    } else {
      GAPID_WARNING("Error during calling function {{$name}}\n");
      return false;
    }
  }
¶
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the logic to call the specified command
-------------------------------------------------------------------------------
*/}}
{{define "Call"}}
  {{AssertType $ "Function"}}

  {{Template "CmdName" $}}(§
    {{range $i, $p := $.CallParameters}}
      {{if $i}}, {{end}}{{$p.Name}}§
    {{end}})
{{end}}

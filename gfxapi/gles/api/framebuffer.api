// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glBindFramebuffer.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glBindFramebuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glBindFramebuffer.xhtml","OpenGL ES 3.1")
cmd void glBindFramebuffer(GLenum target, FramebufferId framebuffer) {
  minRequiredVersion(2, 0)
  switch (target) {
    case GL_FRAMEBUFFER: {
      // version 2.0
    }
    case GL_DRAW_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }

  ctx := GetContext()
  if !(framebuffer in ctx.Instances.Framebuffers) {
    ctx.Instances.Framebuffers[framebuffer] = new!Framebuffer()
  }
  if target == GL_FRAMEBUFFER {
    ctx.BoundFramebuffers[GL_READ_FRAMEBUFFER] = framebuffer
    ctx.BoundFramebuffers[GL_DRAW_FRAMEBUFFER] = framebuffer
  } else {
    ctx.BoundFramebuffers[target] = framebuffer
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glBindRenderbuffer.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glBindRenderbuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glBindRenderbuffer.xhtml","OpenGL ES 3.1")
cmd void glBindRenderbuffer(GLenum target, RenderbufferId renderbuffer) {
  minRequiredVersion(2, 0)
  switch (target) {
    case GL_RENDERBUFFER: {
      // version 2.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }

  ctx := GetContext()
  if !(renderbuffer in ctx.Instances.Renderbuffers) {
    ctx.Instances.Renderbuffers[renderbuffer] = new!Renderbuffer()
  }
  ctx.BoundRenderbuffers[target] = renderbuffer
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glBlitFramebuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glBlitFramebuffer.xhtml","OpenGL ES 3.1")
cmd void glBlitFramebuffer(GLint      srcX0,
                           GLint      srcY0,
                           GLint      srcX1,
                           GLint      srcY1,
                           GLint      dstX0,
                           GLint      dstY0,
                           GLint      dstX1,
                           GLint      dstY1,
                           GLbitfield mask,
                           GLenum     filter) {
  minRequiredVersion(3, 0)
  supportsBits(mask, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT)
  if (GL_COLOR_BUFFER_BIT in mask) {
  }
  if (GL_DEPTH_BUFFER_BIT in mask) {
  }
  if (GL_STENCIL_BUFFER_BIT in mask) {
  }
  switch (filter) {
    case GL_LINEAR, GL_NEAREST: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(filter)
    }
  }

}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glCheckFramebufferStatus.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glCheckFramebufferStatus.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glCheckFramebufferStatus.xhtml","OpenGL ES 3.1")
cmd GLenum glCheckFramebufferStatus(GLenum target) {
  minRequiredVersion(2, 0)
  switch (target) {
    case GL_FRAMEBUFFER: {
      // version 2.0
    }
    case GL_DRAW_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }

  return ?
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glClear.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClear.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClear.xhtml","OpenGL ES 3.1")
cmd void glClear(GLbitfield mask) {
  minRequiredVersion(2, 0)
  supportsBits(mask, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT)
  if (GL_COLOR_BUFFER_BIT in mask) {
  }
  if (GL_DEPTH_BUFFER_BIT in mask) {
  }
  if (GL_STENCIL_BUFFER_BIT in mask) {
  }

  // TODO: use(Clearing.ClearColor) when mask[GL_COLOR_BUFFER_BIT] == true
  if (GL_COLOR_BUFFER_BIT in mask) {
    // color := BoundFramebuffers[GL_FRAMEBUFFER].Attachments[GL_COLOR_ATTACHMENT0]
    // error("Attempting to clear missing color buffer") if !exists(color)
    // modifies(color.Levels[0].Data) // COMPILATION ERROR
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClearBuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClearBuffer.xhtml","OpenGL ES 3.1")
cmd void glClearBufferfi(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) {
  minRequiredVersion(3, 0)
  switch (buffer) {
    case GL_DEPTH_STENCIL: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(buffer)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClearBuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClearBuffer.xhtml","OpenGL ES 3.1")
cmd void glClearBufferfv(GLenum buffer, GLint drawbuffer, const GLfloat* value) {
  minRequiredVersion(3, 0)
  switch (buffer) {
    case GL_COLOR, GL_DEPTH: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(buffer)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClearBuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClearBuffer.xhtml","OpenGL ES 3.1")
cmd void glClearBufferiv(GLenum buffer, GLint drawbuffer, const GLint* value) {
  minRequiredVersion(3, 0)
  switch (buffer) {
    case GL_COLOR, GL_STENCIL: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(buffer)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClearBuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClearBuffer.xhtml","OpenGL ES 3.1")
cmd void glClearBufferuiv(GLenum buffer, GLint drawbuffer, const GLuint* value) {
  minRequiredVersion(3, 0)
  switch (buffer) {
    case GL_COLOR: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(buffer)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glClearColor.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClearColor.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClearColor.xhtml","OpenGL ES 3.1")
cmd void glClearColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  ctx.Clearing.ClearColor = Color(r, g, b, a)
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glClearDepthf.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClearDepthf.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClearDepthf.xhtml","OpenGL ES 3.1")
cmd void glClearDepthf(GLfloat depth) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  ctx.Clearing.ClearDepth = depth
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glClearStencil.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClearStencil.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClearStencil.xhtml","OpenGL ES 3.1")
cmd void glClearStencil(GLint stencil) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  ctx.Clearing.ClearStencil = stencil
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glColorMask.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glColorMask.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glColorMask.xhtml","OpenGL ES 3.1")
cmd void glColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  ctx.Rasterizing.ColorMaskRed = red
  ctx.Rasterizing.ColorMaskGreen = green
  ctx.Rasterizing.ColorMaskBlue = blue
  ctx.Rasterizing.ColorMaskAlpha = alpha
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glDeleteFramebuffers.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glDeleteFramebuffers.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glDeleteFramebuffers.xhtml","OpenGL ES 3.1")
cmd void glDeleteFramebuffers(GLsizei count, const FramebufferId* framebuffers) {
  minRequiredVersion(2, 0)

  f := framebuffers[0:count]
  ctx := GetContext()
  for i in (0 .. count) {
    ctx.Instances.Framebuffers[f[i]] = null
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glDeleteRenderbuffers.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glDeleteRenderbuffers.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glDeleteRenderbuffers.xhtml","OpenGL ES 3.1")
cmd void glDeleteRenderbuffers(GLsizei count, const RenderbufferId* renderbuffers) {
  minRequiredVersion(2, 0)

  r := renderbuffers[0:count]
  ctx := GetContext()
  for i in (0 .. count) {
    ctx.Instances.Renderbuffers[r[i]] = null
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glDepthMask.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glDepthMask.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glDepthMask.xhtml","OpenGL ES 3.1")
cmd void glDepthMask(GLboolean enabled) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  ctx.Rasterizing.DepthMask = enabled
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glFramebufferParameteri.xhtml","OpenGL ES 3.1")
cmd void glFramebufferParameteri(GLenum target, GLenum pname, GLint param) {
  minRequiredVersion(3, 1)
  switch (target) {
    case GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      // version 3.1
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  switch (pname) {
    case GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS, GL_FRAMEBUFFER_DEFAULT_HEIGHT,
        GL_FRAMEBUFFER_DEFAULT_SAMPLES, GL_FRAMEBUFFER_DEFAULT_WIDTH: {
      // version 3.1
    }
    default: {
      glErrorInvalidEnum(pname)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glFramebufferRenderbuffer.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glFramebufferRenderbuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glFramebufferRenderbuffer.xhtml","OpenGL ES 3.1")
cmd void glFramebufferRenderbuffer(GLenum         framebuffer_target,
                                   GLenum         framebuffer_attachment,
                                   GLenum         renderbuffer_target,
                                   RenderbufferId renderbuffer) {
  minRequiredVersion(2, 0)
  switch (framebuffer_target) {
    case GL_FRAMEBUFFER: {
      // version 2.0
    }
    case GL_DRAW_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(framebuffer_target)
    }
  }
  switch (framebuffer_attachment) {
    case GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT, GL_STENCIL_ATTACHMENT: {
      // version 2.0
    }
    case GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11, GL_COLOR_ATTACHMENT12,
        GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14, GL_COLOR_ATTACHMENT15, GL_COLOR_ATTACHMENT2,
        GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6,
        GL_COLOR_ATTACHMENT7, GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9,
        GL_DEPTH_STENCIL_ATTACHMENT: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(framebuffer_attachment)
    }
  }
  switch (renderbuffer_target) {
    case GL_RENDERBUFFER: {
      // version 2.0
    }
    default: {
      glErrorInvalidEnum(renderbuffer_target)
    }
  }

  ctx := GetContext()
  target := switch (framebuffer_target) {
    case GL_FRAMEBUFFER:      as!GLenum(GL_DRAW_FRAMEBUFFER)
    case GL_DRAW_FRAMEBUFFER: as!GLenum(GL_DRAW_FRAMEBUFFER)
    case GL_READ_FRAMEBUFFER: as!GLenum(GL_READ_FRAMEBUFFER)
  }
  framebufferId := ctx.BoundFramebuffers[target]
  framebuffer := ctx.Instances.Framebuffers[framebufferId]
  attachment := framebuffer.Attachments[framebuffer_attachment]
  if (renderbuffer == 0) {
    attachment.Type = GL_NONE
  } else {
    attachment.Type = GL_RENDERBUFFER
  }
  attachment.Object = as!u32(renderbuffer)
  attachment.TextureLevel = 0
  attachment.CubeMapFace = GL_TEXTURE_CUBE_MAP_POSITIVE_X
  framebuffer.Attachments[framebuffer_attachment] = attachment
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glFramebufferTexture2D.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glFramebufferTexture2D.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glFramebufferTexture2D.xhtml","OpenGL ES 3.1")
cmd void glFramebufferTexture2D(GLenum    framebuffer_target,
                                GLenum    framebuffer_attachment,
                                GLenum    texture_target,
                                TextureId texture,
                                GLint     level) {
  minRequiredVersion(2, 0)
  switch (framebuffer_target) {
    case GL_FRAMEBUFFER: {
      // version 2.0
    }
    case GL_DRAW_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(framebuffer_target)
    }
  }
  switch (framebuffer_attachment) {
    case GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT, GL_STENCIL_ATTACHMENT: {
      // version 2.0
    }
    case GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11, GL_COLOR_ATTACHMENT12,
        GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14, GL_COLOR_ATTACHMENT15, GL_COLOR_ATTACHMENT2,
        GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6,
        GL_COLOR_ATTACHMENT7, GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9,
        GL_DEPTH_STENCIL_ATTACHMENT: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(framebuffer_attachment)
    }
  }
  switch (texture_target) {
    case GL_TEXTURE_2D, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, GL_TEXTURE_CUBE_MAP_POSITIVE_X,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_POSITIVE_Z: {
      // version 2.0
    }
    case GL_TEXTURE_2D_MULTISAMPLE: {
      minRequiredVersion(3, 1)
    }
    default: {
      glErrorInvalidEnum(texture_target)
    }
  }

  ctx := GetContext()
  target := switch (framebuffer_target) {
    case GL_FRAMEBUFFER:      as!GLenum(GL_DRAW_FRAMEBUFFER)
    case GL_DRAW_FRAMEBUFFER: as!GLenum(GL_DRAW_FRAMEBUFFER)
    case GL_READ_FRAMEBUFFER: as!GLenum(GL_READ_FRAMEBUFFER)
  }
  framebufferId := ctx.BoundFramebuffers[target]
  framebuffer := ctx.Instances.Framebuffers[framebufferId]
  attachment := framebuffer.Attachments[framebuffer_attachment]
  if (texture == 0) {
    attachment.Type = GL_NONE
    attachment.Object = 0
    attachment.TextureLevel = 0
    attachment.CubeMapFace = GL_TEXTURE_CUBE_MAP_POSITIVE_X
  } else {
    attachment.Type = GL_TEXTURE
    attachment.Object = as!u32(texture)
    attachment.TextureLevel = level
    attachment.CubeMapFace = switch (texture_target) {
      case GL_TEXTURE_2D:                  GL_TEXTURE_CUBE_MAP_POSITIVE_X
      case GL_TEXTURE_CUBE_MAP_POSITIVE_X: GL_TEXTURE_CUBE_MAP_POSITIVE_X
      case GL_TEXTURE_CUBE_MAP_POSITIVE_Y: GL_TEXTURE_CUBE_MAP_POSITIVE_Y
      case GL_TEXTURE_CUBE_MAP_POSITIVE_Z: GL_TEXTURE_CUBE_MAP_POSITIVE_Z
      case GL_TEXTURE_CUBE_MAP_NEGATIVE_X: GL_TEXTURE_CUBE_MAP_NEGATIVE_X
      case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y: GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
      case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z: GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    }
  }
  framebuffer.Attachments[framebuffer_attachment] = attachment
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glFramebufferTextureLayer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glFramebufferTextureLayer.xhtml","OpenGL ES 3.1")
cmd void glFramebufferTextureLayer(GLenum    target,
                                   GLenum    attachment,
                                   TextureId texture,
                                   GLint     level,
                                   GLint     layer) {
  minRequiredVersion(3, 0)
  switch (target) {
    case GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  switch (attachment) {
    case GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11,
        GL_COLOR_ATTACHMENT12, GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14, GL_COLOR_ATTACHMENT15,
        GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5,
        GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7, GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9,
        GL_DEPTH_ATTACHMENT, GL_DEPTH_STENCIL_ATTACHMENT, GL_STENCIL_ATTACHMENT: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(attachment)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glGenFramebuffers.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGenFramebuffers.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGenFramebuffers.xhtml","OpenGL ES 3.1")
cmd void glGenFramebuffers(GLsizei count, FramebufferId* framebuffers) {
  minRequiredVersion(2, 0)

  f := framebuffers[0:count]
  ctx := GetContext()
  for i in (0 .. count) {
    id := as!FramebufferId(?)
    ctx.Instances.Framebuffers[id] = new!Framebuffer()
    f[i] = id
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glGenRenderbuffers.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGenRenderbuffers.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGenRenderbuffers.xhtml","OpenGL ES 3.1")
cmd void glGenRenderbuffers(GLsizei count, RenderbufferId* renderbuffers) {
  minRequiredVersion(2, 0)

  r := renderbuffers[0:count]
  ctx := GetContext()
  for i in (0 .. count) {
    id := as!RenderbufferId(?)
    ctx.Instances.Renderbuffers[id] = new!Renderbuffer()
    r[i] = id
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glGetFramebufferAttachmentParameteriv.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGetFramebufferAttachmentParameteriv.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGetFramebufferAttachmentParameteriv.xhtml","OpenGL ES 3.1")
cmd void glGetFramebufferAttachmentParameteriv(GLenum framebuffer_target,
                                               GLenum attachment,
                                               GLenum parameter,
                                               GLint* value) {
  minRequiredVersion(2, 0)
  switch (framebuffer_target) {
    case GL_FRAMEBUFFER: {
      // version 2.0
    }
    case GL_DRAW_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(framebuffer_target)
    }
  }
  switch (attachment) {
    case GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT, GL_STENCIL_ATTACHMENT: {
      // version 2.0
    }
    case GL_BACK, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11,
        GL_COLOR_ATTACHMENT12, GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14, GL_COLOR_ATTACHMENT15,
        GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5,
        GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7, GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9,
        GL_DEPTH, GL_DEPTH_STENCIL_ATTACHMENT, GL_STENCIL: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(attachment)
    }
  }
  switch (parameter) {
    case GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
        GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE, GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL: {
      // version 2.0
    }
    case GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE, GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE,
        GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING, GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE,
        GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE, GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE,
        GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE, GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE,
        GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(parameter)
    }
  }

  ctx := GetContext()
  target := switch (framebuffer_target) {
    case GL_FRAMEBUFFER:      as!GLenum(GL_DRAW_FRAMEBUFFER)
    case GL_DRAW_FRAMEBUFFER: as!GLenum(GL_DRAW_FRAMEBUFFER)
    case GL_READ_FRAMEBUFFER: as!GLenum(GL_READ_FRAMEBUFFER)
  }
  framebufferId := ctx.BoundFramebuffers[target]
  framebuffer := ctx.Instances.Framebuffers[framebufferId]
  a := framebuffer.Attachments[attachment]
  value[0] = switch (parameter) {
    case GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE:           as!s32(a.Type)
    case GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME:           as!s32(a.Object)
    case GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL:         as!s32(a.TextureLevel)
    case GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE: as!s32(a.CubeMapFace)
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGetFramebufferParameteriv.xhtml","OpenGL ES 3.1")
cmd void glGetFramebufferParameteriv(GLenum target, GLenum pname, GLint* params) {
  minRequiredVersion(3, 1)
  switch (target) {
    case GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER, GL_READ_FRAMEBUFFER: {
      // version 3.1
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  switch (pname) {
    case GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS, GL_FRAMEBUFFER_DEFAULT_HEIGHT,
        GL_FRAMEBUFFER_DEFAULT_SAMPLES, GL_FRAMEBUFFER_DEFAULT_WIDTH: {
      // version 3.1
    }
    default: {
      glErrorInvalidEnum(pname)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glGetRenderbufferParameteriv.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGetRenderbufferParameteriv.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGetRenderbufferParameteriv.xhtml","OpenGL ES 3.1")
cmd void glGetRenderbufferParameteriv(GLenum target, GLenum parameter, GLint* values) {
  minRequiredVersion(2, 0)
  switch (target) {
    case GL_RENDERBUFFER: {
      // version 2.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  switch (parameter) {
    case GL_RENDERBUFFER_ALPHA_SIZE, GL_RENDERBUFFER_BLUE_SIZE, GL_RENDERBUFFER_DEPTH_SIZE,
        GL_RENDERBUFFER_GREEN_SIZE, GL_RENDERBUFFER_HEIGHT, GL_RENDERBUFFER_INTERNAL_FORMAT,
        GL_RENDERBUFFER_RED_SIZE, GL_RENDERBUFFER_STENCIL_SIZE, GL_RENDERBUFFER_WIDTH: {
      // version 2.0
    }
    case GL_RENDERBUFFER_SAMPLES: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(parameter)
    }
  }

  ctx := GetContext()
  id := ctx.BoundRenderbuffers[target]
  rb := ctx.Instances.Renderbuffers[id]
  values[0] = switch (parameter) {
    case GL_RENDERBUFFER_WIDTH:           rb.Width
    case GL_RENDERBUFFER_HEIGHT:          rb.Height
    case GL_RENDERBUFFER_INTERNAL_FORMAT: as!s32(rb.Format)
  // TODO:
  // case GL_RENDERBUFFER_RED_SIZE:        ?
  // case GL_RENDERBUFFER_GREEN_SIZE:      ?
  // case GL_RENDERBUFFER_BLUE_SIZE:       ?
  // case GL_RENDERBUFFER_ALPHA_SIZE:      ?
  // case GL_RENDERBUFFER_DEPTH_SIZE:      ?
  // case GL_RENDERBUFFER_STENCIL_SIZE:    ?
  }
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glInvalidateFramebuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glInvalidateFramebuffer.xhtml","OpenGL ES 3.1")
cmd void glInvalidateFramebuffer(GLenum target, GLsizei count, const GLenum* attachments) {
  minRequiredVersion(3, 0)
  switch (target) {
    case GL_FRAMEBUFFER: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }

}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glInvalidateSubFramebuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glInvalidateSubFramebuffer.xhtml","OpenGL ES 3.1")
cmd void glInvalidateSubFramebuffer(GLenum        target,
                                    GLsizei       numAttachments,
                                    const GLenum* attachments,
                                    GLint         x,
                                    GLint         y,
                                    GLsizei       width,
                                    GLsizei       height) {
  minRequiredVersion(3, 0)
  switch (target) {
    case GL_FRAMEBUFFER: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glIsFramebuffer.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glIsFramebuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glIsFramebuffer.xhtml","OpenGL ES 3.1")
cmd bool glIsFramebuffer(FramebufferId framebuffer) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  return framebuffer in ctx.Instances.Framebuffers
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glIsRenderbuffer.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glIsRenderbuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glIsRenderbuffer.xhtml","OpenGL ES 3.1")
cmd bool glIsRenderbuffer(RenderbufferId renderbuffer) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  return renderbuffer in ctx.Instances.Renderbuffers
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glReadBuffer.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glReadBuffer.xhtml","OpenGL ES 3.1")
cmd void glReadBuffer(GLenum src) {
  minRequiredVersion(3, 0)
  switch (src) {
    case GL_BACK, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT10,
        GL_COLOR_ATTACHMENT11, GL_COLOR_ATTACHMENT12, GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14,
        GL_COLOR_ATTACHMENT15, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4,
        GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7, GL_COLOR_ATTACHMENT8,
        GL_COLOR_ATTACHMENT9, GL_NONE: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(src)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glReadPixels.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glReadPixels.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glReadPixels.xhtml","OpenGL ES 3.1")
cmd void glReadPixels(GLint   x,
                      GLint   y,
                      GLsizei width,
                      GLsizei height,
                      GLenum  format,
                      GLenum  type,
                      void*   data) {
  minRequiredVersion(2, 0)
  switch (format) {
    case GL_ALPHA, GL_RGB, GL_RGBA: {
      // version 2.0
    }
    case GL_LUMINANCE, GL_LUMINANCE_ALPHA, GL_RED, GL_RED_INTEGER, GL_RG, GL_RGBA_INTEGER,
        GL_RGB_INTEGER, GL_RG_INTEGER: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(format)
    }
  }
  switch (type) {
    case GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT_4_4_4_4, GL_UNSIGNED_SHORT_5_5_5_1,
        GL_UNSIGNED_SHORT_5_6_5: {
      // version 2.0
    }
    case GL_BYTE, GL_FLOAT, GL_HALF_FLOAT, GL_INT, GL_UNSIGNED_INT,
        GL_UNSIGNED_INT_10F_11F_11F_REV, GL_UNSIGNED_INT_2_10_10_10_REV,
        GL_UNSIGNED_INT_5_9_9_9_REV: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(type)
    }
  }

  write(data[0:imageSize(as!u32(width), as!u32(height), as!GLenum(format), type)])
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glRenderbufferStorage.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glRenderbufferStorage.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glRenderbufferStorage.xhtml","OpenGL ES 3.1")
cmd void glRenderbufferStorage(GLenum target, GLenum format, GLsizei width, GLsizei height) {
  minRequiredVersion(2, 0)
  switch (target) {
    case GL_RENDERBUFFER: {
      // version 2.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  switch (format) {
    case GL_DEPTH_COMPONENT16, GL_RGB565, GL_RGB5_A1, GL_RGBA4, GL_STENCIL_INDEX8: {
      // version 2.0
    }
    case GL_DEPTH24_STENCIL8, GL_DEPTH32F_STENCIL8, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT32F,
        GL_R16I, GL_R16UI, GL_R32I, GL_R32UI, GL_R8, GL_R8I, GL_R8UI, GL_RG16I, GL_RG16UI,
        GL_RG32I, GL_RG32UI, GL_RG8, GL_RG8I, GL_RG8UI, GL_RGB10_A2, GL_RGB10_A2UI, GL_RGB8,
        GL_RGBA16I, GL_RGBA16UI, GL_RGBA32I, GL_RGBA32UI, GL_RGBA8, GL_RGBA8I, GL_RGBA8UI,
        GL_SRGB8_ALPHA8: {
      minRequiredVersion(3, 0)
    }
    default: {
      glErrorInvalidEnum(format)
    }
  }

  ctx := GetContext()
  id := ctx.BoundRenderbuffers[target]
  rb := ctx.Instances.Renderbuffers[id]
  rb.Format = format
  rb.Width = width
  rb.Height = height
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glRenderbufferStorageMultisample.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glRenderbufferStorageMultisample.xhtml","OpenGL ES 3.1")
cmd void glRenderbufferStorageMultisample(GLenum  target,
                                          GLsizei samples,
                                          GLenum  format,
                                          GLsizei width,
                                          GLsizei height) {
  minRequiredVersion(3, 0)
  switch (target) {
    case GL_RENDERBUFFER: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(target)
    }
  }
  switch (format) {
    case GL_DEPTH24_STENCIL8, GL_DEPTH32F_STENCIL8, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT24,
        GL_DEPTH_COMPONENT32F, GL_R16I, GL_R16UI, GL_R32I, GL_R32UI, GL_R8, GL_R8I, GL_R8UI,
        GL_RG16I, GL_RG16UI, GL_RG32I, GL_RG32UI, GL_RG8, GL_RG8I, GL_RG8UI, GL_RGB10_A2,
        GL_RGB10_A2UI, GL_RGB565, GL_RGB5_A1, GL_RGB8, GL_RGBA16I, GL_RGBA16UI, GL_RGBA32I,
        GL_RGBA32UI, GL_RGBA4, GL_RGBA8, GL_RGBA8I, GL_RGBA8UI, GL_SRGB8_ALPHA8, GL_STENCIL_INDEX8:
    {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(format)
    }
  }

}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glStencilMask.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glStencilMask.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glStencilMask.xhtml","OpenGL ES 3.1")
cmd void glStencilMask(GLuint mask) {
  minRequiredVersion(2, 0)

  ctx := GetContext()
  ctx.Rasterizing.StencilMask[GL_FRONT] = mask
  ctx.Rasterizing.StencilMask[GL_BACK] = mask
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man/xhtml/glStencilMaskSeparate.xml","OpenGL ES 2.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glStencilMaskSeparate.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glStencilMaskSeparate.xhtml","OpenGL ES 3.1")
cmd void glStencilMaskSeparate(GLenum face, GLuint mask) {
  minRequiredVersion(2, 0)
  switch (face) {
    case GL_BACK, GL_FRONT, GL_FRONT_AND_BACK: {
      // version 2.0
    }
    default: {
      glErrorInvalidEnum(face)
    }
  }

  ctx := GetContext()
  switch (face) {
    case GL_FRONT:          ctx.Rasterizing.StencilMask[GL_FRONT] = mask
    case GL_BACK:           ctx.Rasterizing.StencilMask[GL_BACK] = mask
    case GL_FRONT_AND_BACK: {
      ctx.Rasterizing.StencilMask[GL_FRONT] = mask
      ctx.Rasterizing.StencilMask[GL_BACK] = mask
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
// Automatically generated file. Do not modify!
////////////////////////////////////////////////////////////////////////////////

package test

import (
	"android.googlesource.com/platform/tools/gpu/atom"
	"android.googlesource.com/platform/tools/gpu/binary/schema"
)

func init() {
	sc_CmdClone := schema.Of((*CmdClone)(nil).Class())
	sc_CmdClone.Metadata = append(sc_CmdClone.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdClone",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdMake := schema.Of((*CmdMake)(nil).Class())
	sc_CmdMake.Metadata = append(sc_CmdMake.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdMake",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdCopy := schema.Of((*CmdCopy)(nil).Class())
	sc_CmdCopy.Metadata = append(sc_CmdCopy.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdCopy",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdCharsliceToString := schema.Of((*CmdCharsliceToString)(nil).Class())
	sc_CmdCharsliceToString.Metadata = append(sc_CmdCharsliceToString.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdCharsliceToString",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdCharptrToString := schema.Of((*CmdCharptrToString)(nil).Class())
	sc_CmdCharptrToString.Metadata = append(sc_CmdCharptrToString.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdCharptrToString",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdSliceCasts := schema.Of((*CmdSliceCasts)(nil).Class())
	sc_CmdSliceCasts.Metadata = append(sc_CmdSliceCasts.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdSliceCasts",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoid := schema.Of((*CmdVoid)(nil).Class())
	sc_CmdVoid.Metadata = append(sc_CmdVoid.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoid",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdUnknownRet := schema.Of((*CmdUnknownRet)(nil).Class())
	sc_CmdUnknownRet.Metadata = append(sc_CmdUnknownRet.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdUnknownRet",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdUnknownWritePtr := schema.Of((*CmdUnknownWritePtr)(nil).Class())
	sc_CmdUnknownWritePtr.Metadata = append(sc_CmdUnknownWritePtr.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdUnknownWritePtr",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdUnknownWriteSlice := schema.Of((*CmdUnknownWriteSlice)(nil).Class())
	sc_CmdUnknownWriteSlice.Metadata = append(sc_CmdUnknownWriteSlice.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdUnknownWriteSlice",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidU8 := schema.Of((*CmdVoidU8)(nil).Class())
	sc_CmdVoidU8.Metadata = append(sc_CmdVoidU8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidU8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidS8 := schema.Of((*CmdVoidS8)(nil).Class())
	sc_CmdVoidS8.Metadata = append(sc_CmdVoidS8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidS8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidU16 := schema.Of((*CmdVoidU16)(nil).Class())
	sc_CmdVoidU16.Metadata = append(sc_CmdVoidU16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidU16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidS16 := schema.Of((*CmdVoidS16)(nil).Class())
	sc_CmdVoidS16.Metadata = append(sc_CmdVoidS16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidS16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidF32 := schema.Of((*CmdVoidF32)(nil).Class())
	sc_CmdVoidF32.Metadata = append(sc_CmdVoidF32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidF32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidU32 := schema.Of((*CmdVoidU32)(nil).Class())
	sc_CmdVoidU32.Metadata = append(sc_CmdVoidU32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidU32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidS32 := schema.Of((*CmdVoidS32)(nil).Class())
	sc_CmdVoidS32.Metadata = append(sc_CmdVoidS32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidS32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidF64 := schema.Of((*CmdVoidF64)(nil).Class())
	sc_CmdVoidF64.Metadata = append(sc_CmdVoidF64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidF64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidU64 := schema.Of((*CmdVoidU64)(nil).Class())
	sc_CmdVoidU64.Metadata = append(sc_CmdVoidU64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidU64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidS64 := schema.Of((*CmdVoidS64)(nil).Class())
	sc_CmdVoidS64.Metadata = append(sc_CmdVoidS64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidS64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidBool := schema.Of((*CmdVoidBool)(nil).Class())
	sc_CmdVoidBool.Metadata = append(sc_CmdVoidBool.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidBool",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidString := schema.Of((*CmdVoidString)(nil).Class())
	sc_CmdVoidString.Metadata = append(sc_CmdVoidString.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidString",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoid3Strings := schema.Of((*CmdVoid3Strings)(nil).Class())
	sc_CmdVoid3Strings.Metadata = append(sc_CmdVoid3Strings.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoid3Strings",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoid3InArrays := schema.Of((*CmdVoid3InArrays)(nil).Class())
	sc_CmdVoid3InArrays.Metadata = append(sc_CmdVoid3InArrays.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoid3InArrays",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidInArrayOfPointers := schema.Of((*CmdVoidInArrayOfPointers)(nil).Class())
	sc_CmdVoidInArrayOfPointers.Metadata = append(sc_CmdVoidInArrayOfPointers.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidInArrayOfPointers",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadU8 := schema.Of((*CmdVoidReadU8)(nil).Class())
	sc_CmdVoidReadU8.Metadata = append(sc_CmdVoidReadU8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadU8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadS8 := schema.Of((*CmdVoidReadS8)(nil).Class())
	sc_CmdVoidReadS8.Metadata = append(sc_CmdVoidReadS8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadS8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadU16 := schema.Of((*CmdVoidReadU16)(nil).Class())
	sc_CmdVoidReadU16.Metadata = append(sc_CmdVoidReadU16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadU16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadS16 := schema.Of((*CmdVoidReadS16)(nil).Class())
	sc_CmdVoidReadS16.Metadata = append(sc_CmdVoidReadS16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadS16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadF32 := schema.Of((*CmdVoidReadF32)(nil).Class())
	sc_CmdVoidReadF32.Metadata = append(sc_CmdVoidReadF32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadF32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadU32 := schema.Of((*CmdVoidReadU32)(nil).Class())
	sc_CmdVoidReadU32.Metadata = append(sc_CmdVoidReadU32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadU32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadS32 := schema.Of((*CmdVoidReadS32)(nil).Class())
	sc_CmdVoidReadS32.Metadata = append(sc_CmdVoidReadS32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadS32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadF64 := schema.Of((*CmdVoidReadF64)(nil).Class())
	sc_CmdVoidReadF64.Metadata = append(sc_CmdVoidReadF64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadF64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadU64 := schema.Of((*CmdVoidReadU64)(nil).Class())
	sc_CmdVoidReadU64.Metadata = append(sc_CmdVoidReadU64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadU64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadS64 := schema.Of((*CmdVoidReadS64)(nil).Class())
	sc_CmdVoidReadS64.Metadata = append(sc_CmdVoidReadS64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadS64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadBool := schema.Of((*CmdVoidReadBool)(nil).Class())
	sc_CmdVoidReadBool.Metadata = append(sc_CmdVoidReadBool.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadBool",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidReadPtrs := schema.Of((*CmdVoidReadPtrs)(nil).Class())
	sc_CmdVoidReadPtrs.Metadata = append(sc_CmdVoidReadPtrs.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidReadPtrs",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteU8 := schema.Of((*CmdVoidWriteU8)(nil).Class())
	sc_CmdVoidWriteU8.Metadata = append(sc_CmdVoidWriteU8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteU8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteS8 := schema.Of((*CmdVoidWriteS8)(nil).Class())
	sc_CmdVoidWriteS8.Metadata = append(sc_CmdVoidWriteS8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteS8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteU16 := schema.Of((*CmdVoidWriteU16)(nil).Class())
	sc_CmdVoidWriteU16.Metadata = append(sc_CmdVoidWriteU16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteU16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteS16 := schema.Of((*CmdVoidWriteS16)(nil).Class())
	sc_CmdVoidWriteS16.Metadata = append(sc_CmdVoidWriteS16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteS16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteF32 := schema.Of((*CmdVoidWriteF32)(nil).Class())
	sc_CmdVoidWriteF32.Metadata = append(sc_CmdVoidWriteF32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteF32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteU32 := schema.Of((*CmdVoidWriteU32)(nil).Class())
	sc_CmdVoidWriteU32.Metadata = append(sc_CmdVoidWriteU32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteU32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteS32 := schema.Of((*CmdVoidWriteS32)(nil).Class())
	sc_CmdVoidWriteS32.Metadata = append(sc_CmdVoidWriteS32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteS32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteF64 := schema.Of((*CmdVoidWriteF64)(nil).Class())
	sc_CmdVoidWriteF64.Metadata = append(sc_CmdVoidWriteF64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteF64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteU64 := schema.Of((*CmdVoidWriteU64)(nil).Class())
	sc_CmdVoidWriteU64.Metadata = append(sc_CmdVoidWriteU64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteU64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteS64 := schema.Of((*CmdVoidWriteS64)(nil).Class())
	sc_CmdVoidWriteS64.Metadata = append(sc_CmdVoidWriteS64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteS64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWriteBool := schema.Of((*CmdVoidWriteBool)(nil).Class())
	sc_CmdVoidWriteBool.Metadata = append(sc_CmdVoidWriteBool.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWriteBool",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidWritePtrs := schema.Of((*CmdVoidWritePtrs)(nil).Class())
	sc_CmdVoidWritePtrs.Metadata = append(sc_CmdVoidWritePtrs.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidWritePtrs",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdU8 := schema.Of((*CmdU8)(nil).Class())
	sc_CmdU8.Metadata = append(sc_CmdU8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdU8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdS8 := schema.Of((*CmdS8)(nil).Class())
	sc_CmdS8.Metadata = append(sc_CmdS8.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdS8",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdU16 := schema.Of((*CmdU16)(nil).Class())
	sc_CmdU16.Metadata = append(sc_CmdU16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdU16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdS16 := schema.Of((*CmdS16)(nil).Class())
	sc_CmdS16.Metadata = append(sc_CmdS16.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdS16",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdF32 := schema.Of((*CmdF32)(nil).Class())
	sc_CmdF32.Metadata = append(sc_CmdF32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdF32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdU32 := schema.Of((*CmdU32)(nil).Class())
	sc_CmdU32.Metadata = append(sc_CmdU32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdU32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdS32 := schema.Of((*CmdS32)(nil).Class())
	sc_CmdS32.Metadata = append(sc_CmdS32.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdS32",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdF64 := schema.Of((*CmdF64)(nil).Class())
	sc_CmdF64.Metadata = append(sc_CmdF64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdF64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdU64 := schema.Of((*CmdU64)(nil).Class())
	sc_CmdU64.Metadata = append(sc_CmdU64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdU64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdS64 := schema.Of((*CmdS64)(nil).Class())
	sc_CmdS64.Metadata = append(sc_CmdS64.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdS64",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdBool := schema.Of((*CmdBool)(nil).Class())
	sc_CmdBool.Metadata = append(sc_CmdBool.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdBool",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdString := schema.Of((*CmdString)(nil).Class())
	sc_CmdString.Metadata = append(sc_CmdString.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdString",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdPointer := schema.Of((*CmdPointer)(nil).Class())
	sc_CmdPointer.Metadata = append(sc_CmdPointer.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdPointer",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoid3Remapped := schema.Of((*CmdVoid3Remapped)(nil).Class())
	sc_CmdVoid3Remapped.Metadata = append(sc_CmdVoid3Remapped.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoid3Remapped",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidInArrayOfRemapped := schema.Of((*CmdVoidInArrayOfRemapped)(nil).Class())
	sc_CmdVoidInArrayOfRemapped.Metadata = append(sc_CmdVoidInArrayOfRemapped.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidInArrayOfRemapped",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidOutArrayOfRemapped := schema.Of((*CmdVoidOutArrayOfRemapped)(nil).Class())
	sc_CmdVoidOutArrayOfRemapped.Metadata = append(sc_CmdVoidOutArrayOfRemapped.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidOutArrayOfRemapped",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdVoidOutArrayOfUnknownRemapped := schema.Of((*CmdVoidOutArrayOfUnknownRemapped)(nil).Class())
	sc_CmdVoidOutArrayOfUnknownRemapped.Metadata = append(sc_CmdVoidOutArrayOfUnknownRemapped.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdVoidOutArrayOfUnknownRemapped",
		Flags:            0,
		DocumentationUrl: "[]",
	})

	sc_CmdRemapped := schema.Of((*CmdRemapped)(nil).Class())
	sc_CmdRemapped.Metadata = append(sc_CmdRemapped.Metadata, &atom.Metadata{
		API:              apiID,
		DisplayName:      "cmdRemapped",
		Flags:            0,
		DocumentationUrl: "[]",
	})
}

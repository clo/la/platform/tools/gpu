// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package path

import (
	"fmt"

	"android.googlesource.com/platform/tools/gpu/binary"
)

// Device is a path that refers to a device.
type Device struct {
	binary.Generate
	ID binary.ID // The device's unique identifier.
}

// String returns the string representation of the path.
func (c *Device) String() string { return c.Path() }

// Path implements the Path interface.
func (c *Device) Path() string {
	return fmt.Sprintf("Device(%v)", c.ID)
}

// Base implements the Path interface, returning nil as this is a root.
func (c *Device) Base() Path {
	return nil
}

// Clone implements the Path interface, returning a deep-copy of this path.
func (c *Device) Clone() Path {
	return &Device{ID: c.ID}
}

// Validate implements the Path interface.
func (c *Device) Validate() error {
	switch {
	case c == nil:
		return fmt.Errorf("Device is nil")
	case !c.ID.Valid():
		return fmt.Errorf("Device.ID is invalid")
	}
	return nil
}

// FindDevice returns the first Device found traversing the path p.
// If no Device was found, then nil is returned.
func FindDevice(p Path) *Device {
	for p != nil {
		if p, ok := p.(*Device); ok {
			return p
		}
		p = p.Base()
	}
	return nil
}
